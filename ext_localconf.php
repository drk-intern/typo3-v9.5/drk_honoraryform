<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_honoraryform',
    'Honoraryform',
    [
        \DRK\DrkHonoraryform\Controller\HonoraryFormController::class => 'showHonoraryForm, send',
    ],
    // non-cacheable actions
    [
        \DRK\DrkHonoraryform\Controller\HonoraryFormController::class => 'send',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
