<?php

namespace DRK\DrkHonoraryform\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Domain\Repository\AbstractDrkRepository;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;

/**
 * The repository for Honorarys
 */
class HonoraryFormRepository extends AbstractDrkRepository
{

    /**
     * getHonoraryApplication
     *
     * @param array $aRequest
     * @return mixed
     * @throws NoSuchCacheException
     */

    public function getHonoraryApplication(array $aRequest = []): mixed
    {
        $cacheIdentifier = sha1(json_encode(['drk_honoraryform|getHonoraryApplication', $aRequest]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        try {
            $result = $this->executeJsonClientAction('getHonoraryApplication', $aRequest);

            $this->getCache()->set($cacheIdentifier, $result, [], $this->heavy_cache);

            return $result;
        } catch (\Exception $exception) {
            $this->error = ['Fehler' => $exception->getMessage()];
            return false;
        }
    }

    /**
     *
     * sendHonoraryData
     *
     * @param array $aRequest
     * @return boolean|array
     */
    public function sendHonoraryData(array $aRequest = []): bool|array
    {
        try {
            $aResult = $this->executeJsonClientAction('sendHonoraryData', $aRequest);

            // Result is NULL, if no errors occur
            if (empty($aResult)) {
                return true;
            } else {
                return $aResult;
            }
        } catch (\Exception $exception) {
            $this->error = ['Fehler' => $exception->getMessage()];
            return false;
        }
    }
}
