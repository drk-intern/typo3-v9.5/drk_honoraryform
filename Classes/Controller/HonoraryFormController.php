<?php

namespace DRK\DrkHonoraryform\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of    <f:flashMessages renderMode="div" />
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Controller\AbstractDrkController;
use DRK\DrkGeneral\Utilities\Utility;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use DRK\DrkHonoraryform\Domain\Repository\HonoraryFormRepository;
use TYPO3\CMS\Core\Page\AssetCollector;

/**
 * HonoraryController
 */
class HonoraryFormController extends AbstractDrkController
{

    /**
     * honoraryFormRepository
     *
     * @var HonoraryFormRepository $honoraryFormRepository
     */
    protected HonoraryFormRepository $honoraryFormRepository;

    /**
     * @var array
     */
    protected $mandatoryFormFields = [
        'person' => ['vorname', 'name'],
    ];

    /**
     * Application Array
     *
     * @var array|bool
     */
    protected array|bool $ApplicationArray = [];

    /**
     * Error Message
     *
     * @var array
     */
    protected array $aError = [];

    /**
     * @param AssetCollector $assetCollector
     */
    public function __construct(
        private readonly AssetCollector $assetCollector
    ) {
    }

    /**
     * Init
     *
     * @return void
     */
    protected function initializeAction(): void
    {
        parent::initializeAction();

        $this->assetCollector->addStyleSheet(
            'Honoraryform',
            'EXT:drk_honoraryform/Resources/Public/Css/styles.css'
        );

        $this->assetCollector->addJavaScript(
            'Honoraryform',
            'EXT:drk_honoraryform/Resources/Public/Scripts/tx_drkhonoraryform.js',
            [],
            ['priority' => false] // lädt in footer

        );

    }




    /**
     * @param HonoraryFormRepository $conactFormRepository
     */
    public function injectHonoraryRepository(HonoraryFormRepository $honoraryFormRepository): void
    {
        $this->honoraryFormRepository = $honoraryFormRepository;
    }

    /**
     * action show
     *
     * @return ResponseInterface
     * @throws NoSuchCacheException
     */
    public function showHonoraryFormAction():ResponseInterface
    {
        $aRequest = [
            $this->settings['apiKey'],
        ];
        if (!$this->ApplicationArray = $this->honoraryFormRepository->getHonoraryApplication($aRequest)) {
            $this->ApplicationArray = array(0 => 'Bitte legen sie Eigenschaften in der Aktivenverwaltung an!');
        }

        // if Error, then show it now

        if ($this->aError = $this->honoraryFormRepository->getErrors()) {
            $this->view->assign('error', true);
            $this->view->assign('errorMessages', $this->aError);
        } else {
            $this->view->assign('error', false);
        }

        $this->view->assign('prefix_array', Utility::$prefixArray);
        $this->view->assign('title_array', Utility::$titleArray);
        $this->view->assign('application_array', $this->ApplicationArray);
        $this->view->assign(
            'privacy_url',
            $this->validateUrl($this->settings['privacyUrl']) ? $this->settings['privacyUrl'] : ''
        );
        return $this->htmlResponse();
    }


    /**
     * action send
     *
     * @return void|ResponseInterface
     * @throws NoSuchCacheException
     */
    public function sendAction():ResponseInterface
    {
        $aFormData = [];
        $this->view->assign('sending_ok', false);
        $this->view->assign('debug', $this->settings['debug']);
        $aArguments = $this->request->getArguments()['honoraryform'];

        $errors = false;

        //check honeypot field
        if (!$this->isHoneypotFilled($aArguments['birthname']))
        {
            $errors = true;
        }

        if (!empty($aArguments)) {
            array_key_exists(
                'anrede',
                $aArguments
            ) ? $aFormData['anrede'] = $aArguments['anrede'] : $aFormData['anrede'] = 1;
            array_key_exists(
                'titel',
                $aArguments
            ) ? $aFormData['titel'] = $aArguments['titel'] : $aFormData['titel'] = 0;
            array_key_exists('name', $aArguments) ? $aFormData['name'] = $aArguments['name'] : $aFormData['name'] = "";
            array_key_exists(
                'vorname',
                $aArguments
            ) ? $aFormData['vorname'] = $aArguments['vorname'] : $aFormData['vorname'] = "";
            array_key_exists(
                'strasse',
                $aArguments
            ) ? $aFormData['strasse'] = $aArguments['strasse'] : $aFormData['strasse'] = "";
            array_key_exists('ort', $aArguments) ? $aFormData['ort'] = $aArguments['ort'] : $aFormData['ort'] = "";
            array_key_exists('plz', $aArguments) ? $aFormData['plz'] = $aArguments['plz'] : $aFormData['plz'] = "";
            array_key_exists(
                'telefon',
                $aArguments
            ) ? $aFormData['telefon'] = $aArguments['telefon'] : $aFormData['telefon'] = "";
            array_key_exists(
                'email',
                $aArguments
            ) ? $aFormData['email'] = $aArguments['email'] : $aFormData['email'] = "";

            // skills and values
            array_key_exists(
                'drivers_license',
                $aArguments
            ) && $aArguments['drivers_license'] ? $aFormData['drivers_license'] = 1 : $aFormData['drivers_license'] = 0;
            array_key_exists(
                'children',
                $aArguments
            ) && $aArguments['children'] ? $aFormData['children'] = 1 : $aFormData['children'] = 0;
            array_key_exists(
                'youths',
                $aArguments
            ) && $aArguments['youths'] ? $aFormData['youths'] = 1 : $aFormData['youths'] = 0;
            array_key_exists('men', $aArguments) && $aArguments['men'] ? $aFormData['men'] = 1 : $aFormData['men'] = 0;
            array_key_exists(
                'women',
                $aArguments
            ) && $aArguments['women'] ? $aFormData['women'] = 1 : $aFormData['women'] = 0;
            array_key_exists(
                'no_preference',
                $aArguments
            ) && $aArguments['no_preference'] ? $aFormData['no_preference'] = 1 : $aFormData['no_preference'] = 0;

            array_key_exists(
                'preferred_activity',
                $aArguments
            ) ? $aFormData['preferred_activity'] = $aArguments['preferred_activity'] : $aFormData['preferred_activity'] = "";

            array_key_exists(
                'hours_a_day',
                $aArguments
            ) && $aArguments['hours_a_day'] ? $aFormData['hours_a_day'] = intval($aArguments['hours_a_day']) : $aFormData['hours_a_day'] = 0;
            array_key_exists(
                'hours_a_week',
                $aArguments
            ) && $aArguments['hours_a_week'] ? $aFormData['hours_a_week'] = intval($aArguments['hours_a_week']) : $aFormData['hours_a_week'] = 0;
            array_key_exists(
                'hours_a_month',
                $aArguments
            ) && $aArguments['hours_a_month'] ? $aFormData['hours_a_month'] = intval($aArguments['hours_a_month']) : $aFormData['hours_a_month'] = 0;

            array_key_exists(
                'abilities',
                $aArguments
            ) ? $aFormData['abilities'] = $aArguments['abilities'] : $aFormData['abilities'] = "";

            //TODO: this field not exist at the form
            array_key_exists(
                'message',
                $aArguments
            ) ? $aFormData['message'] = $aArguments['message'] : $aFormData['message'] = "";

            array_key_exists(
                'weekdays',
                $aArguments
            ) ? $aFormData['weekdays'] = $aArguments['weekdays'] : $aFormData['weekdays'] = "";

            array_key_exists(
                'morning',
                $aArguments
            ) && $aArguments['morning'] ? $aFormData['morning'] = 1 : $aFormData['morning'] = 0;
            array_key_exists(
                'afternoon',
                $aArguments
            ) && $aArguments['afternoon'] ? $aFormData['afternoon'] = 1 : $aFormData['afternoon'] = 0;
            array_key_exists(
                'evening',
                $aArguments
            ) && $aArguments['evening'] ? $aFormData['evening'] = 1 : $aFormData['evening'] = 0;

            // go through applications

            if (array_key_exists('applications', $aArguments) && is_array($aArguments['applications'])) {
                $aRequest = [
                    $this->settings['apiKey'],
                ];

                $this->ApplicationArray = $this->honoraryFormRepository->getHonoraryApplication($aRequest);


                foreach ($aArguments['applications'] as $iApplicationsKey) {
                    if (array_key_exists(intval($iApplicationsKey), $this->ApplicationArray)) {
                        $aFormData['applications'][] = intval($iApplicationsKey);
                    }
                }
            }
        }


        //check if we have a empty form
        $sCheck = $aArguments['name'] . $aArguments['vorname'] . $aArguments['email'];
        if (empty($sCheck)) {
            $this->error = array('Fehler' => 'Bitte kein leeres Formular absenden!');
            $errors = true;
        }

        $mandatoryFieldsFilled = $this->checkMandatoryFormFields($this->mandatoryFormFields['person'], $aFormData);

        if (!$errors && $mandatoryFieldsFilled) {
            $request = [
                $this->settings['apiKey'],
                $aFormData
            ];

            // sending Data are successful
            if ($this->honoraryFormRepository->sendHonoraryData($request)) {
                $this->view->assign('sending_ok', true);

                // if successPageId is set, the redirect to this page
                if ($this->settings['successPageId'] && !$this->settings['debug']) {
                    $uriBuilder = $this->uriBuilder;
                    $uriBuilder->reset();
                    $uriBuilder->setTargetPageUid($this->settings['successPageId']);
                    $uriBuilder->setCreateAbsoluteUri(true);
                    $uri = $uriBuilder->build();
                    return $this->responseFactory->createResponse(
                        303,
                        ''
                    )->withAddedHeader('Location', $uri);
                } // else go on an render the template
                else {
                    unset($request);
                    $aClientData = $aFormData;
                    $aClientData['anrede'] = array_key_exists(
                        $aFormData['anrede'],
                        Utility::$prefixArray
                    ) ? Utility::$prefixArray[$aFormData['anrede']] : "Herr";
                    $aClientData['titel'] = array_key_exists(
                        $aFormData['titel'],
                        Utility::$titleArray
                    ) ? Utility::$titleArray[$aFormData['titel']] : "";

                    $this->view->assign('aClientData', $aClientData);
                }
            }
        }

        // show debug
        if ($this->settings['debug']) {
            $this->view->assign('debug', $aArguments);
        }

        // if Error, then show it now
        if ($aError = $this->honoraryFormRepository->getErrors()) {
            $this->error = $aError;
            $errors = true;
        }

        $this->view->assign('error', false);
        if ($errors) {
            $this->view->assign('error', true);
            $this->view->assign('errorMessages', array_merge($this->error));
        }
        return $this->htmlResponse();
    }
}
