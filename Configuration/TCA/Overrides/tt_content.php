<?php

$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_honoraryform',
    'Honoraryform',
    'LLL:EXT:drk_honoraryform/Resources/Private/Language/locallang.xlf:tt_content.honoraryform_plugin.title',
    'EXT:drk_honoraryform/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Aktivenanmeldung'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);
